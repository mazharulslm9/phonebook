<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phonebook</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./../../../asset/css/font-awesome.min.css" rel="stylesheet">

        <style>
            a {
                font-size: 16px;
                color: #f5f5f5;
            }
            a:hover{

                text-decoration: none;
                color: #fff;
            }
        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    </head>
    <body class="bg-info">
        <section>
            <div class="container">
                <div class="row ">
                    <div style="margin-top: 100px;border-radius: 10px;padding: 10px 20px 30px 20px;" class="col-md-offset-4 col-md-4 bg-warning">


                        <h3 class="text-center text-success"><span><i style="font-size: 24px;margin-right: 10px" class="fa fa-phone text-center text-success"></i></span>Phone Book</h3>
                        <hr style="border-top: 1px solid #e2e2e2;">
                        <form class="form-group text-center" action="store.php" method="post">
                            <div class="form-group">
                                <label for="exampleInputName2" class="text-success">EnterName: </label>
                                <input type="text" name="name"class="form-control" id="exampleInputName2" placeholder="Enter Name" required="">
                                <label for="exampleInputName2" class="text-success">Enter Phone Number: </label>
                                <input type="number" name="number"class="form-control" id="exampleInputName2" placeholder="+880" required="">

                            </div>

                            <button type="submit" class="btn btn-success btn-md" >Save</button>
<!--                            <button type="submit" class="btn btn-success btn-md" >Save & Add</button>-->
                            <button type="reset" class="btn btn-success btn-md" >Reset</button>
                        </form>

                        <nav style="padding: 0px 10px 0px 10px" class="text-center list-unstyled list-inline">
                            <li class="pull-left"><button class="btn btn-sm btn-primary"> <a href="javascript:history.go(-1)">Back</a></button></li>
                            <li class="pull-right"><button class="btn btn-sm btn-primary"> <a href="list.php">Go to List</a></button></li>

                        </nav>
                    </div>
                </div>
            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>
