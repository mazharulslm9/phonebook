<?php
ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once './../../../vendor/autoload.php';

use App\DebugAndDie\PhoneBook\Phone;
use App\DebugAndDie\Utility\Utility;

$phone = new Phone();
$phones = $phone->show($_GET['id']);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phone Book</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./../../../asset/css/font-awesome.min.css" rel="stylesheet">
        <style>
            a {
                font-size: 16px;
                color: #f5f5f5;
            }
            a:hover{

                text-decoration: none;
                color: #fff;
            }
            tr:hover {
                background-color: #c0c0c0 ;

            }
        </style>
        <style>
            table {
                background-color: #c8e5bc;
                width: 100%;
                text-align: center;


            }
            th {
                text-align: center;

                padding: 5px;
            }
            td {
                text-align: center;
                padding: 15px;

            }
        </style>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">

                    <div style="margin-top: 100px;border-radius: 10px;padding: 10px 20px 30px 20px;" class="col-md-offset-4 col-md-4 bg-warning">
                        <h3 class="text-center text-success"><span><i style="font-size: 24px;margin-right: 10px" class="fa fa-phone text-center text-success"></i>View PhoneBook</h3>
                        <hr style="border-top: 1px solid #e2e2e2;">
                        <table class="table-bordered">
                            <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Phone Number</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td >
                                        <?php echo $phones['id']; ?>
                                    </td>

                                    <td >
                                        <?php echo $phones['name']; ?>
                                    </td>

                                    <td>
                                        <?php echo $phones['number']; ?>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
                <nav style="padding: 30px" class="text-center list-unstyled list-inline">
                    <li ><button class="btn btn-sm btn-primary"> <a href="javascript:history.go(-1)">Back</a></button></li>
                    <li ><button class="btn btn-sm btn-primary"> <a href="index.php">Go to List</a></button></li>

                </nav>
            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>
    </body>
</html>