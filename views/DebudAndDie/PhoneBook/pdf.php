
<?php

ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once './../../../vendor/autoload.php';

use App\DebugAndDie\PhoneBook\Phone;
use App\DebugAndDie\Utility\Utility;

$phone = new Phone();
$phones = $phone->index();
$trs = "";
?>
<?php

$slno = 0;
foreach ($phones as $ph):
    $slno++;
    $trs .="<tr>";
    $trs .="<td>" . $slno . "</td>";
    $trs .="<td>" . $ph->name . "</td>";
    $trs .="<td>" . $ph->number . "</td>";
    $trs .="</tr>";
endforeach;
?>
<?php

$html = <<<mazhar



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phone Book</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <ins><h3 class="text-center">Phone Number List</h3></ins> 


                <hr>

            
                <div class="row">


                    <table class="table table-bordered text-center bg-info">
                        <thead>
                            <tr>

                                <th class="text-center">Sl.</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Phone Number &dArr;</th>

                            </tr>
                        </thead>
                        <tbody>

                     echo $trs;        
                        </tbody>
                    </table>
                </div>


        </section>

      
    </body>
</html>
mazhar;
?>
<?php

//require_once $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "phone" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'mpdf.php';
require_once './../../../vendor/mpdf/mpdf/mpdf.php';
$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
