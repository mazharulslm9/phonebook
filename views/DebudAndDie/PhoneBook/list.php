
<?php
ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once './../../../vendor/autoload.php';

use App\DebugAndDie\PhoneBook\Phone;
use App\DebugAndDie\Utility\Utility;

$phone = new Phone();
$phones = $phone->index();


?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phone Book</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
    

        <style>
            a {
                font-size: 16px;
                color: #f5f5f5;
            }
            a:hover{

                text-decoration: none;
                color: #fff;
            }
            .no a {
                color: #000;
            }
            #utility{

                float:right;
                width:60%;

            }
            #message {
                background: #66afe9;
                color: #fff;
                margin: 15px;

            }

        </style>
        



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <ins><h3 class="text-center">Phone Number List</h3></ins> 
                <div style="padding: 15px 0px 15px 0px;" class="row bg-success">
                    <div class="col-md-2">
                        <button  class="btn btn-success btn-md"> <a href="create.php">Add Phone Number</a></button>
                    </div>
                    <div class="col-md-2">

                        <select style="width: 50%">
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                            <option>40</option>
                            <option>50</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <span><button  class="btn btn-info btn-sm"><a href="pdf.php">Download as PDF</a></button></span>  
                        <span> <button  class="btn btn-info btn-sm"><a href="xl.php">XL</a></button></span>


                    </div>

                    <div class="col-md-2">
                        <button  class="btn btn-danger btn-md pull-right"><a href="trashed.php">All Trashed Number's</a></button>

                    </div>
                    <form  class="form-inline"action="search.php?go" method="post" id="searchform">
                        <div class="col-md-3">
                            <div class="input-group">
                                <input type="text" id="search" name="name" class="form-control" placeholder="Search with names...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" name="submit" value="search" type="submit">Go!</button>
                                </span>
                            </div> 
                        </div> 
                    </form>
                </div>

                <hr>

                <div id="message" >
                    <h3> <?php echo Utility::message(); ?></h3> 

                </div>
                <div class="row">


                    <table class="table table-bordered text-center bg-info">
                        <thead>
                            <tr>

                                <th class="text-center">Sl.</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Phone Number &dArr;</th>
                                <th class="text-center" colspan="4">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            if (count($phones) > 0) {
                                $slno = 1;
                                foreach ($phones as $phone) {
                                    ?>
                                    <tr>

                                        <td><?php echo $slno; ?></td>
                                        <td class="no"><a href="show.php?id=<?php echo $phone->id; ?>"><?php echo $phone->name; ?></a></td>
                                        <td class="no" style="text-transform: uppercase;"><a href="show.php?id=<?php echo $phone->id; ?>"><?php echo $phone->number; ?></a></td>
                                        <td><button class="btn btn-success btn-md"><a href="show.php?id=<?php echo $phone->id; ?>">View</a></button></td>
                                        <td><button class="btn btn-info btn-md"><a href="edit.php?id=<?php echo $phone->id; ?>">Edit</a></button></td>



                                <form action="delete.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $phone->id; ?>"/>
                                    <td><button type="submit" class="btn btn-danger btn-md delete">Delete</button></td>
                                </form>
                        <td><button id="trash" class="btn btn-warning btn-md"><a href="trash.php?id=<?php echo $phone->id; ?>">Trash</a></button></td>



                                </tr>
                                <?php
                                $slno++;
                            }
                        } else {
                            ?>
                            <tr>
                                <td colspan="8">
                                    Nothing in the list!!!
                                </td>

                            </tr>


                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>

                <div class="row text-center">      
                    <nav>

                        <ul class="pagination my-navigation">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <button class="btn btn-danger btn-md"><a href="./../../../index.php"> Home </a></button>

                </div>

        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>
    


        <script>
            $('.delete').bind('click', function (e) {

                var deleteItem = confirm("Are You Sure You Want to Delete?");
                if (!deleteItem) {

                    //return false;
                    e.preventDefault();
                }


            });
            $('#message').fadeOut(6000);



        </script>


    </body>
</html>
