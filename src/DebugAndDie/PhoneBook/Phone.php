<?php

namespace App\DebugAndDie\PhoneBook;

use App\DebugAndDie\Utility\Utility;

class Phone {

    public $id = " ";
    public $name = " ";
    public $number = " ";
    public $username = " ";
    public $password = " ";
    public $fname = " ";
    public $sname = " ";
    public $deleted_at = null;

    public function __construct($data = false) {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("phonebook") or die("Cannot select database.");
        if (is_array($data) && array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        $this->name = $data['name'];
        $this->number = $data['number'];
        $this->username = $data['username'];
        $this->password = $data['password'];
        $this->fname = $data['fname'];
        $this->sname = $data['sname'];
    }

    public function login() {
        session_start();
        $message = "";
        if (count($_POST) > 0) {
            $result = mysql_query("SELECT * FROM users WHERE username='" . $_POST["username"] . "' and password = '" . $_POST["password"] . "'");
            $row = mysql_fetch_array($result);
            if (is_array($row)) {
                header("Location:create.php");
            } else {
                 Utility::message("Invalid Username OR Password !!!");
            }
        }
     
    }

    public function show($id = false) {


        $query = "SELECT * FROM `phone_tbl` WHERE id=" . $id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function index() {
        $phone = array();
        $query = "SELECT * FROM `phone_tbl` WHERE `deleted_at` IS NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $phone[] = $row;
        }
        return $phone;
    }

    public function search() {
        $phone = array();
        if (isset($_POST['submit'])) {
            if (isset($_GET['go'])) {
                if (preg_match("/^[  a-zA-Z]+/", $_POST['name'])) {
                    $name = $_POST['name'];
//                    $db = mysql_connect("localhost", "root", "") or die('I cannot connect to the database  because: ' . mysql_error());
//                    $mydb = mysql_select_db("phonebook");
                    $sql = "SELECT  name, number FROM phone_tbl WHERE name LIKE '%" . $name . "%' OR number LIKE '%" . $name . "%'";
                    $result = mysql_query($sql);
                    while ($row = mysql_fetch_array($result)) {
                        $phone[] = $row;
                    }
                } else {
                    Utility::message("Please Search With Name!!");
                }
                return $phone;
            }
        }
    }

    public function store() {



        $query = "INSERT INTO `phonebook`.`phone_tbl` (`name`,`number`) VALUES ('" . $this->name . "','" . $this->number . "');";

        $result = mysql_query($query);


        if ($result) {
            Utility::message("<strong>$this->name</strong> your phone number is added successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('list.php');
    }

    public function register() {
        $message = "";


        $query = "INSERT INTO `phonebook`.`users` (`username`, `password`, `firstname`, `surename`) VALUES ( '" . $this->username . "', '" . $this->password . "', '" . $this->fname . "', '" . $this->sname . "');";
        $result = mysql_query($query);

        if ($result) {
            $message = "ok.";
        } else {
            $message = "Please try again later!!.";
        }

        Utility::redirect('index.php');
    }

    public function update() {


        $query = "UPDATE `phonebook`.`phone_tbl` SET `name` = '" . $this->name . "',`number` = '" . $this->number . "' WHERE `phone_tbl`.`id` = " . $this->id;
        $result = mysql_query($query);

        if ($result) {
            Utility::message("phone number is edited successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('list.php');
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            Utility::message("No Id Found!!");
            return Utility::redirect("index.php");
        }


        $query = "DELETE FROM `phonebook`.`phone_tbl` WHERE `phone_tbl`.`id` = " . $id;
        $result = mysql_query($query);


        if ($result) {
            Utility::message("Phone number is deleted successfully.");
        } else {
            Utility::message("There is an error while deleting title. Please try again later.");
        }

        Utility::redirect('list.php');
    }

    public function trash($id = NULL) {
        if (is_null($id)) {
            Utility::message("No Id Found!!");
            return Utility::redirect("list.php");
        }
        $this->id = $id;
        $this->deleted_at = time();

        $query = "UPDATE `phonebook`.`phone_tbl` SET `deleted_at` = '" . $this->deleted_at . "' WHERE `phone_tbl`.`id` = " . $this->id;
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Phone number is trashed successfully.");
        } else {
            Utility::message("There is an error while trashing phone number. Please try again later.");
        }

        Utility::redirect('list.php');
    }

    public function trashed() {
        $phone = array();



        $query = "SELECT * FROM `phone_tbl` WHERE `deleted_at` IS NOT NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $phone[] = $row;
        }
        return $phone;
    }

    public function recover($id = NULL) {
        if (is_null($id)) {
            Utility::message("No Id Found!!");
            return Utility::redirect("list.php");
        }
        $this->id = $id;


        $query = "UPDATE `phonebook`.`phone_tbl` SET `deleted_at` = NULL WHERE `phone_tbl`.`id` = " . $this->id;
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Phone number is recoverd successfully.");
        } else {
            Utility::message("There is an error while recovering phone number. Please try again later.");
        }

        Utility::redirect('list.php');
    }

    public function recovermultiple($ids = array()) {
        if (is_array($ids) && count($ids) > 0) {
            $_ids = implode(",", $ids);

            $query = "UPDATE `phonebook`.`phone_tbl` SET `deleted_at` = NULL WHERE `phone_tbl`.`id` IN ($_ids) ";
            $result = mysql_query($query);
            if ($result) {
                Utility::message("Phone number is recoverd successfully.");
            } else {
                Utility::message("There is an error while recovering phone number. Please try again later.");
            }

            Utility::redirect('list.php');
        } else {
            Utility::message("No Id Found!!");
            return Utility::redirect("list.php");
        }
    }

    public function deletemultiple($ids = array()) {
        if (is_array($ids) && count($ids) > 0) {
            $_ids = implode(",", $ids);

            $query = "DELETE FROM `phonebook`.`phone_tbl` WHERE `phone_tbl`.`id` IN($_ids) ";
            $result = mysql_query($query);


            if ($result) {
                Utility::message("Phone number is deleted successfully.");
            } else {
                Utility::message("There is an error while deleting title. Please try again later.");
            }

            Utility::redirect('trashed.php');
        } else {
            Utility::message("No Id Found!!");
            return Utility::redirect("trashed.php");
        }
    }

}
