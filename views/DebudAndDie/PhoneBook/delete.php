<?php

ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once './../../../vendor/autoload.php';

use App\DebugAndDie\PhoneBook\Phone;
use App\DebugAndDie\Utility\Utility;

$phone = new Phone();

$phone->delete($_REQUEST['id']);
